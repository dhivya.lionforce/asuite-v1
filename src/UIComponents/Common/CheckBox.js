import React from 'react';

function CheckBox({ className = "", lable, id, name }) {
  return (
    <div className={`df ${className}`}>
      <input
        type="checkbox"
        className="magic-checkbox"
        id={id}
        name={name || id}
      />
      <label className="pb-0 fs-14" htmlFor={id}>
        {lable || id}
      </label>
    </div>
  )
}
export default CheckBox