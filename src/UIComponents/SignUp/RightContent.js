import React from "react";

export default function RightContent() {
  return (
    <div className="dc vh-100 login-right signup-con theme-bg">
     <div className="w-100">
     <h1 className='fw-500 mb-20 text-center white-clr d-md-none d-block'>Welcome to <span className='fw-500 warning-clr'>ASuite</span></h1>

      <div className="white-bg br8 inner-section w-100">
        <div className="dc h-100 ">
          <form className="w-100">
            <h2 className="fs-36 fw-700 theme-light-clr mb-45 text-center">Sign In</h2>
            <div class="form-group">
              <label for="FirstName">First Name </label>
              <input type="text" class="form-control" id="FirstName" placeholder="First Name" />
            </div>
            <div class="form-group">
              <label for="LastName">Last Name </label>
              <input type="text" class="form-control" id="LastName" placeholder="Last Name" />
            </div>
            <div class="form-group">
              <label for="Email">Email </label>
              <input type="email" class="form-control" id="Email" placeholder="Email" />
            </div>
            <div class="form-group">
              <label for="Password">Password </label>
              <input type="Password" class="form-control" id="Password" placeholder="Password" />
            </div>
            <div class="form-group">
              <label for="ConfirmPassword">Confirm Password </label>
              <input type="Password" class="form-control" id="ConfirmPassword" placeholder="Confirm Password" />
            </div>
            <div class="form-group">
              <label for="Company">Company</label>
              <input type="text" class="form-control" id="Company" placeholder="Company" />
            </div>
           
            <button className="w-100 warning-btn mb-24 mt-8">Sign Up</button>
            <p className="text-center">
            Already a User ? <span className="theme-light2-clr fs-14 cp"> Login</span>
            </p>
          </form>
        </div>
      </div>
      </div>

    </div>
  );
}
