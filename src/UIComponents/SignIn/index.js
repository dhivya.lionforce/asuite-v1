import React from 'react'
import LeftContent from './LeftContent'
import RightContent from './RightContent'

export default function SignIn() {
    return (
        <section className="vh-100 vw-100 login-con d-flex">
           <LeftContent/> 
            <RightContent/>

        </section>
    )
}
 