import React from 'react'
import Infographics from "../../assets/img/login-info.png";

export default function LeftContent() {
    return (
        <div className='d-none d-md-block  text-center login-left h-100 '>
                <h1 className='fw-500 text-primary'>Welcome to <span className='fw-500 warning-clr'>ASuite</span></h1>
                {/* <img src={Infographics} alt="Infographics" /> */}

                <p className='fs-18 warning-clr' >Powered by Negentis</p>
        </div>
    )
}
