import React from "react";
import CheckBox from "../Common/CheckBox";

export default function RightContent() {
  return (
    <div className=" vh-100 dc h-100 login-right theme-bg">
     <h1 className='fw-500 mb-20 text-center white-clr d-md-none d-block'>Welcome to <span className='fw-500 warning-clr'>ASuite</span></h1>

      <div className="white-bg br8 inner-section">
        <div className="dc h-100 ">
          <form className="w-100">
            <h2 className="fs-36 fw-700 theme-light-clr mb-45 text-center">Sign In</h2>
            <div class="form-group">
              <label for="Email">Email </label>
              <input type="email" class="form-control" id="Email" placeholder="Email" />
            </div>
            <div class="form-group">
              <label for="Password">Password </label>
              <input type="Password" class="form-control" id="Password" placeholder="Password" />
            </div>
            <div class="d-flex justify-content-between mb-20">
              <CheckBox id="Remember" label="Remember Me"/>  
              <p className="theme-light2-clr cp">Forgot Password ?</p>     
            </div>
            <button className="w-100 warning-btn mb-24">Login</button>
            <p className="text-center">
            Not a User?<span className="theme-light2-clr fs-14 cp"> Create Account</span>
            </p>
          </form>
        </div>
      </div>

    </div>
  );
}
