import React from 'react'

export default function Sidebar() {
    return (
        <div className='app-sidebar theme-bg dfc hidden-y'>
            <div className='dc h-73 white-clr fw-600 fs-28 p-16'>ASuite </div>
            <div className='flex1 scroll-y'>
                <div className='app-menus cp white-clr  active'>Role assignment</div>
                <div className='app-menus cp white-clr '>Company Management</div>
                <div className='app-menus cp white-clr '> Project Management</div>
                <div className='app-menus cp white-clr '>Worklist Management</div>
                <div className='app-menus cp white-clr '>Assign Work activities</div>
                <div className='app-menus cp white-clr '>Management of Field activites</div>
                <div className='app-menus cp white-clr '>Milestone approval</div>
                <div className='app-menus cp white-clr '>Webex</div>
                <div className='app-menus cp white-clr '>Feedback</div>

            </div>
        </div>

    )
}
