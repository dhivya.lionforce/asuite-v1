import React from 'react'
import Header from './Header'
import Sidebar from './Sidebar'

export default function Main() {
    return (
        <div className='main-page dfc'>
            <Sidebar/>
            <div className='main-content dfc'>
                <Header/>
            </div>
        </div>
    )
}
