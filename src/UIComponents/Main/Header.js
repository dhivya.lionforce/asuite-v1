import React from "react";

export default function Header() {
  return (
    <div className="d-flex main-header h-73 gray-btm-br justify-content-between align-items-center">
      <div></div>
      <div className="d-flex align-items-center">
        <div className="position-relative">
          <i class="fa fa-bell-o fs-20 info-clr" aria-hidden="true"></i>
          <span className="pulse"></span>
        </div>
      </div>
    </div>
  );
}
